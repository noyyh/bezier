#pragma once
#include "bezier.h"

namespace Bezier
{
namespace Test
{
    ::Bezier::Bezier<3> defaultCubicBezier();
    ::Bezier::Bezier<2> defaultQuadraticBezier();
} // namespace Test
} // namespace Bezier
